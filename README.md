# beetl-jfinal-integration

#### 介绍

从beetl包独立出来，独立整合Jfinal 新版本

#### 安装教程

maven

~~~xml
<dependency>
    <groupId>com.ibeetl</groupId>
    <artifactId>jfinal-integration-4</artifactId>
    <version>${version}</version>
</dependency>
<dependency>
    <groupId>com.ibeetl</groupId>
    <artifactId>beetl</artifactId>
    <version>${beetl-version}</version>
</dependency>
~~~

#### 使用说明

~~~
public void configConstant(Constants me) {
    me.setDevMode(true);
    JFinalBeetlRenderFactory rf = new JFinalBeetlRenderFactory();
    rf.configClassPath("/templates");
    me.setRenderFactory(rf);
    GroupTemplate gt = rf.groupTemplate;
    //其他优化
}
~~~

调用config 来配置模板路径，默认是classpath 根目录，你也可以调用configFilePath指定一个绝对目录
~~~java
rf.configFilePath("c:/project/views");
~~~

Contorller代码如下

~~~java
public class HelloController extends Controller {
  public void index() {
    this.setAttr("name","JFinal World");
    this.render("/hello.html");
  }
}


~~~

注意，JFinal关于模板的大多数配置并不是针对所有模板引擎，比如
~~~java
me.addSharedFunction("/view/common/layout.html");
~~~

Beetl 有类似功能和配置，但不是使用JFinal方式，所以你需要使用Beetl方式，关于Beetl模板更多配置，需要参考Beetl文档，ibeetl.com